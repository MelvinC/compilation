
%%
   
%package compiler
%class Lexer
%public
%line
%column
%cup

%%

[ \t\n\r]				{ }
;						{ return getSymbolFactory().newSymbol("TERM", Sym.TERM); }

new						{ return getSymbolFactory().newSymbol("NEW", Sym.NEW); }
panel					{ return getSymbolFactory().newSymbol("PANEL", Sym.PANEL); }
draw					{ return getSymbolFactory().newSymbol("DRAW", Sym.DRAW); }
rectangle				{ return getSymbolFactory().newSymbol("RECTANGLE", Sym.RECTANGLE); }
circle					{ return getSymbolFactory().newSymbol("CIRCLE", Sym.CIRCLE); }
filled					{ return getSymbolFactory().newSymbol("FILLED", Sym.FILLED); }
with					{ return getSymbolFactory().newSymbol("WITH", Sym.WITH); }
stroked					{ return getSymbolFactory().newSymbol("STROKED", Sym.STROKED); }
as						{ return getSymbolFactory().newSymbol("AS", Sym.AS); }
at						{ return getSymbolFactory().newSymbol("AT", Sym.AT); }
animate					{ return getSymbolFactory().newSymbol("ANIMATE", Sym.ANIMATE); }
"{"						{ return getSymbolFactory().newSymbol("OPEN_BRACKETS", Sym.OPEN_BRACKETS); }
"}"						{ return getSymbolFactory().newSymbol("CLOSE_BRACKETS", Sym.CLOSE_BRACKETS); }
move					{ return getSymbolFactory().newSymbol("MOVE", Sym.MOVE); }
horizontal				{ return getSymbolFactory().newSymbol("HORIZONTAL", Sym.HORIZONTAL, yytext()); }
vertical				{ return getSymbolFactory().newSymbol("VERTICAL", Sym.VERTICAL, yytext()); }
from					{ return getSymbolFactory().newSymbol("FROM", Sym.FROM); }
to						{ return getSymbolFactory().newSymbol("TO", Sym.TO); }
during					{ return getSymbolFactory().newSymbol("DURING", Sym.DURING); }
after					{ return getSymbolFactory().newSymbol("AFTER", Sym.AFTER); }
click					{ return getSymbolFactory().newSymbol("CLICK", Sym.CLICK, yytext()); }
repeat					{ return getSymbolFactory().newSymbol("REPEAT", Sym.REPEAT); }
times					{ return getSymbolFactory().newSymbol("TIMES", Sym.TIMES); }
change					{ return getSymbolFactory().newSymbol("CHANGE", Sym.CHANGE); }
color					{ return getSymbolFactory().newSymbol("COLOR", Sym.COLOR); }
size					{ return getSymbolFactory().newSymbol("SIZE", Sym.SIZE); }
in						{ return getSymbolFactory().newSymbol("IN", Sym.IN); }
"+"						{ return getSymbolFactory().newSymbol("PLUS", Sym.PLUS); }
"-"						{ return getSymbolFactory().newSymbol("MINUS", Sym.MINUS); }
"*"						{ return getSymbolFactory().newSymbol("MULT", Sym.MULT); }
"/"						{ return getSymbolFactory().newSymbol("DIV", Sym.DIV); }
"="						{ return getSymbolFactory().newSymbol("EQUAL", Sym.EQUAL); }
\".[^\"]*\"					{ return getSymbolFactory().newSymbol ("TEXT", Sym.TEXT, yytext().substring(1,yylength()-1)); }
rgb						{ return getSymbolFactory().newSymbol("RGB", Sym.RGB); }
,						{ return getSymbolFactory().newSymbol("COMA", Sym.COMA); }
\(						{ return getSymbolFactory().newSymbol("OPEN_PARENTHESIS", Sym.OPEN_PARENTHESIS); }
\)						{ return getSymbolFactory().newSymbol("CLOSE_PARENTHESIS", Sym.CLOSE_PARENTHESIS); }


[0-9]+					{ return getSymbolFactory().newSymbol("INT", Sym.INT, Double.valueOf(yytext())); }
[a-zA-Z][a-zA-Z0-9_]*	{ return getSymbolFactory().newSymbol("NAME", Sym.NAME, yytext()); }


[^]						{ throw new Error("Illegal character <"+yytext()+">"); }