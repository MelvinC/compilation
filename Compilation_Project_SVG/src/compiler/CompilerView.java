package compiler;

import java.awt.Graphics;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import ast.Visitor;

import ui.AbstractDocument;
import ui.AbstractView;

public class CompilerView extends AbstractView {

	public CompilerView(AbstractDocument document) {
		super(document);
	}

	private static final long serialVersionUID = 1L;

	@Override
	protected void onPaint(Graphics g) {
		Context context = ((CompilerDocument) getDocument()).getContext();
		if ( context != null && ! context.hasErrors()) {
			Visitor v = new Visitor (context.getVista());
			context.getRacine().visit(v);
			File ff=new File(getNameFile()+".html");
			try {
				ff.createNewFile();		
				FileWriter ffw=new FileWriter(ff);
				ffw.write(v.getBuilder());
				ffw.write("\n");
				ffw.close();
				g.drawString("Votre fichier a �t� g�n�r�", 130, 100);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}