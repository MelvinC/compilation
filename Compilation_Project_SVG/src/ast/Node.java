package ast;

import java.util.ArrayList;
import java.util.List;

public class Node {

	private NodeType type;
	private String name;
	private double value;
	private List<Node> children;
	
	public Node(NodeType nt, String n, Double v) {
		this.type = nt;
		this.name = n;
		this.value = v;
		this.children = new ArrayList<Node>();
	}
	
	public Node(NodeType nt, String n) {
		this.type = nt;
		this.name = n;
		this.children = new ArrayList<Node>();
	}
	
	public Node(NodeType nt, Double v) {
		this.type = nt;
		this.value = v;
		this.children = new ArrayList<Node>();
	}
	
	public Node(NodeType nt) {
		this.type = nt;
		this.children = new ArrayList<Node>();
	}
	
	//Autres constructeurs
	
	public NodeType getType() {
		return this.type;
	}
	
	public List<Node> getChildren() {
		return this.children;
	}
	
	public void addChildren(Node n) {
		this.children.add(n);
	}
	
	public void addFirstChildren(Node n) {
		this.children.add(0, n);
	}
	
	public String getName() {
		return this.name;
	}
	
	public double getValue() {
		return this.value;
	}
	
	public void visit(Visitor v) {
		v.visit(this);
	}
}
