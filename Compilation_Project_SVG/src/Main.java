import compiler.CompilerDocument;
import compiler.CompilerView;
import ui.AbstractDocument;
import ui.AbstractView;

public class Main {
	
	private AbstractView view;
	private AbstractDocument document;
	
	public Main() {
		document = new CompilerDocument ();
		view = new CompilerView (document);
		document.setView(view);
	}
	
	public static void main (String argv []) {
		Main app = new Main();
	    app.show();
	}

    public void show () {
    	view.setVisible(true);
    }
}
