package Symbol;

import java.util.Stack;

public class Vista {
	
	private Stack<SymbolTable> stack;
	
	public Vista() {
		this.stack=new Stack<SymbolTable>();
	}
	
	public void pushTable(SymbolTable table) {
		this.stack.push(table);
	}
	
	public SymbolTable topTable() {
		return this.stack.peek();
	}
	
	public void popTable() {
		this.stack.pop();
	}
	
	public boolean exist(String s) {
		for(SymbolTable st : this.stack) {
			if(st.exist(s)) {
				return true;
			}
		}
		return false;
	}
	
	public Symbol get(String s) {
		if(this.exist(s)) {
			for(SymbolTable st : this.stack) {
				if(st.exist(s)) {
					return st.get(s);
				}
			}
		}
		return null;
	}
	
	public void addLocal(String s, Symbol sb) {
		this.topTable().add(s, sb);
	}
	
	public void addLocal(String s, FunctionSymbol fb) {
		this.topTable().add(s, fb);
	}
	
	public boolean existLocal(String s) {
		return this.topTable().exist(s);
	}
}
