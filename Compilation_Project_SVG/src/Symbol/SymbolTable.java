package Symbol;

import java.util.HashMap;
import java.util.Map;

public class SymbolTable {

	Map<String, Symbol> table;
	
	public SymbolTable(){
		table = new HashMap<String, Symbol>();
	}
	
	public boolean exist(String s) {
		return this.table.containsKey(s);
	}
	
	public Symbol get(String s) {
		return this.table.get(s);
	}
	
	public void add(String s, Symbol sb) {
		this.table.put(s, sb);
	}
}
