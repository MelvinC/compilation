package Symbol;

import ast.Node;

public class FunctionSymbol extends Symbol{

	private Node node;
	
	public FunctionSymbol(String name, Node node) {
		super(name);
		this.node=node;
	}
	
	public Node getValue() {
		return this.node;
	}
}
