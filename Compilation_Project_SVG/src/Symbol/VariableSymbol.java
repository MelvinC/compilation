package Symbol;

public class VariableSymbol extends Symbol {
	
	private double value;

	public VariableSymbol(String name, double value) {
		super(name);
		this.value=value;
	}
	
	public Double getValue() {
		return this.value;
	}
}
