package ast;

import java.util.HashMap;
import java.util.Map;

import Symbol.FunctionSymbol;
import Symbol.Symbol;
import Symbol.VariableSymbol;
import Symbol.Vista;

public class Visitor {

	private StringBuilder builder;
	private Vista vista;
	private Map<String, Node> labelShape;
	private String currentID;

	public Visitor(Vista v) {
		this.builder = new StringBuilder();
		this.labelShape = new HashMap<String, Node>();
		this.vista = v;
	}

	public String getBuilder() {
		return this.builder.toString();
	}

	public void visit(Node n) {
		if (n == null) {
			return;
		}
		switch (n.getType()) {
		case NODE_INIT:
			visit_initialisation(n);
			break;
		case NODE_RECT:
			visit_rectangle(n);
			break;
		case NODE_BLOCK:
			visit_block(n);
			break;
		case NODE_CIRCLE:
			visit_circle(n);
			break;
		case NODE_DRAWING:
			visit_drawing(n);
			break;
		case NODE_FILL:
			visit_fill(n);
			break;
		case NODE_STROKE:
			visit_stroke(n);
			break;
		case NODE_CONSTANTE:
			visit_constante(n);
			break;
		case NODE_ANIMATION:
			visit_animation(n);
			break;
		case NODE_STRING:
			visit_string(n);
			break;
		case NODE_MOVEMENT:
			visit_movement(n);
			break;
		case NODE_ACTION:
			visit_action(n);
			break;
		case NODE_ACTION_TO:
			visit_action_to(n);
			break;
		case NODE_COORDINATES:
			visit_coordinates(n);
			break;
		case NODE_TIME:
			visit_time(n);
			break;
		case NODE_EXPRESSION_TIME_PLUS:
			visit_expression_time_plus(n);
			break;
		case NODE_EXPRESSION_TIME_MINUS:
			visit_expression_time_minus(n);
			break;
		case NODE_NEW_CONSTANT:
			visit_new_constant(n);
			break;
		case NODE_NEW_CIRCLE:
			visit_new_circle(n);
			break;
		case NODE_NEW_RECTANGLE:
			visit_new_rectangle(n);
			break;
		case NODE_TEXT:
			visit_text(n);
			break;
		case NODE_LOOP:
			visit_loop(n);
			break;
		case NODE_COLOR_RGB:
			visit_color_rgb(n);
			break;
		case NODE_COLOR_NAME:
			builder.append(visit_string(n));
			break;
		case NODE_CHANGE_COLOR:
			visit_change_color(n);
			break;
		case NODE_ANIMATION_COLOR:
			visit_animation_color(n);
			break;
		default:
			break;
		}

	}

	public void visit_block(Node b) {
		// System.out.println("je visite le block !!!");
		for (Node e : b.getChildren()) {
			visit(e);
		}
	}

	public void visit_initialisation(Node n) {
		// System.out.println("je visite l'initialisation !!!");
		builder.append("<svg ");
		int i = 0;
		for (Node e : n.getChildren()) {
			switch (i) {
			case 0:
				builder.append("width=\"");
				builder.append(visit_constante(e));
				break;
			case 1:
				builder.append("\" height=\"");
				builder.append(visit_constante(e));
				builder.append("\"> \n");
				break;
			default:
				visit(e);
				break;
			}
			i++;
		}
		builder.append(" <svg/>");
	}

	public void visit_rectangle(Node n) {
		int i = 0;
		builder.append("rect ");
		for (Node e : n.getChildren()) {
			switch (i) {
			case 0:
				builder.append("width=\"");
				builder.append(visit_expression(e));
				break;
			case 1:
				builder.append("\" height=\"");
				builder.append(visit_expression(e));
				builder.append("\"");
				break;
			default:
				visit(e);
				break;
			}
			i++;
		}
	}

	public void visit_circle(Node n) {
		int i = 0;
		builder.append("circle ");
		for (Node e : n.getChildren()) {
			switch (i) {
			case 0:
				builder.append("r=\"");
				builder.append(visit_expression(e));
				builder.append("\" ");
				break;
			default:
				visit(e);
				break;
			}
			i++;
		}
	}

	public void visit_text(Node n) {
		int i = 0;
		builder.append(" ");
		for (Node e : n.getChildren()) {
			switch (i) {
			case 0:
				builder.append("r=\"");
				builder.append(visit_expression(e));
				builder.append("\" ");
				break;
			default:
				visit(e);
				break;
			}
			i++;
		}
	}

	public void visit_drawing(Node d) {
		NodeType type = null;
		int i = 0;
		String sText = "";
		builder.append("<");
		for (Node e : d.getChildren()) {
			switch (i) {
			case 0:
				type = e.getType();
				if (type.equals(NodeType.NODE_STRING)) {
					visit((Node) this.vista.get(visit_string(e)).getValue());
				} else {
					if (type.equals(NodeType.NODE_TEXT)) {
						builder.append("text");
						sText = e.getName();
						visit_text(e);
					} else {
						visit(e);
					}
				}
				break;
			case 1:
				builder.append(" ");
				if (type.equals(NodeType.NODE_CIRCLE))
					builder.append("c");
				builder.append("x=\"");
				builder.append(visit_expression(e));
				break;
			case 2:
				builder.append("\" ");
				if (type.equals(NodeType.NODE_CIRCLE))
					builder.append("c");
				builder.append("y=\"");
				builder.append(visit_expression(e));
				builder.append("\"");
				break;
			default:
				visit(e);
				break;
			}
			i++;
		}
		if (d.getName() != null) {
			labelShape.put(d.getName(), d);
			builder.append(" id=\"" + d.getName() + "\"");
			builder.append("/>\n");
		} else {
			if (type == NodeType.NODE_TEXT) {
				builder.append(">" + sText + "</text>\n");
			} else {
				builder.append("/>\n");
			}
		}
	}

	public void visit_fill(Node f) {
		builder.append(" fill=\"");
		if (f.getChildren().get(0).getType() == NodeType.NODE_COLOR_NAME)
			builder.append(f.getChildren().get(0).getName());
		if (f.getChildren().get(0).getType() == NodeType.NODE_COLOR_RGB)
			visit(f.getChildren().get(0));
		builder.append("\"");
	}

	public void visit_stroke(Node s) {
		builder.append(" stroke=\"");
		if (s.getChildren().get(0).getType() == NodeType.NODE_COLOR_NAME)
			builder.append(s.getChildren().get(0).getName());
		if (s.getChildren().get(0).getType() == NodeType.NODE_COLOR_RGB)
			visit(s.getChildren().get(0));
		builder.append("\"");
	}

	public double visit_expression(Node n) {
		double r = 0;
		switch (n.getType()) {
		case NODE_CONSTANTE:
			r = visit_constante(n);
			break;
		case NODE_PLUS:
			r = visit_expression(n.getChildren().get(0)) + visit_expression(n.getChildren().get(1));
			break;
		case NODE_MINUS:
			r = visit_expression(n.getChildren().get(0)) - visit_expression(n.getChildren().get(1));
			break;
		case NODE_MULT:
			r = visit_expression(n.getChildren().get(0)) * visit_expression(n.getChildren().get(1));
			break;
		case NODE_DIV:
			try {
				r = visit_expression(n.getChildren().get(0)) / visit_expression(n.getChildren().get(1));
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		case NODE_STRING:
			r = (Double) this.vista.get(visit_string(n)).getValue();
			break;
		}
		return r;
	}

	public double visit_constante(Node c) {
		return c.getValue();
	}

	public void visit_animation(Node n) {
		int i = 0;
		String id = "";
		for (Node e : n.getChildren()) {
			switch (i) {
			case 0:
				id = "xlink:href=\"#";
				id += visit_string(e);
				currentID = visit_string(e);
				id += "\" ";
				break;
			default:
				builder.append("<animate ");
				if (visit_string(n) != null) {
					builder.append("id=\"");
					builder.append(visit_string(n));
					builder.append("\" ");
				}
				builder.append(id);
				visit(e);
				break;
			}
			i++;
		}
	}

	public String visit_string(Node n) {
		return n.getName();
	}

	public void visit_movement(Node n) {
		int i = 0;
		for (Node e : n.getChildren()) {
			switch (i) {
			case 0:
				visit(e);
				break;
			case 1:
				builder.append("dur=\"");
				builder.append(visit_expression(e));
				builder.append("s\"");
				break;
			default:
				builder.append(" begin=\"");
				visit(e);
				builder.append("\"");
				break;
			}
			i++;
		}
		builder.append(" fill=\"freeze\"/>\n");

	}

	public void visit_action(Node n) {
		int i = 0;
		for (Node e : n.getChildren()) {
			switch (i) {
			case 0:
				visit(e);
				break;
			case 1:
				builder.append("from=\"");
				builder.append(visit_expression(e));
				builder.append("\"");
				break;
			default:
				builder.append(" to=\"");
				builder.append(visit_expression(e));
				builder.append("\" ");
				break;
			}
			i++;
		}
	}
	
	public void visit_action_to(Node n) {
		int i = 0;
		for (Node e : n.getChildren()) {
			switch (i) {
			case 0:
				visit(e);
				break;
			default:
				builder.append("from=\"");
				builder.append(visit_expression(labelShape.get(currentID).getChildren().get(1)));
				builder.append("\"");
				builder.append(" to=\"");
				builder.append(visit_expression(e));
				builder.append("\" ");
				break;
			}
			i++;
		}
	}

	public void visit_coordinates(Node n) {
		builder.append("attributeName=\"");
		if (labelShape.get(currentID).getChildren().get(0).getType().equals(NodeType.NODE_CIRCLE))
			builder.append("c");
		if (n.getName().equals("horizontal"))
			builder.append("x");
		else if (n.getName().equals("vertical"))
			builder.append("y");
		builder.append("\" ");
	}

	public void visit_time(Node n) {
		if (!n.getChildren().isEmpty()) {
			Node c = n.getChildren().get(0);
			switch (c.getType()) {
			case NODE_TIME_CLICK:
				builder.append(visit_string(c));
				break;
			case NODE_TIME_VALUE:
				builder.append(visit_constante(c));
				builder.append("s");
				break;
			case NODE_TIME:
				for (Node e : c.getChildren())
					builder.append(visit_string(e));
					builder.append(".end");
				break;
			}
		}
	}

	public void visit_expression_time_plus(Node n) {
		Node c = n.getChildren().get(0);
		// System.out.println(c.getType());
		switch (c.getType()) {
		case NODE_TIME_CLICK:
			builder.append(visit_string(c));
			builder.append("+");
			builder.append(visit_constante(n));
			builder.append("s");
			break;
		case NODE_TIME_VALUE:
			builder.append(visit_constante(c) + visit_constante(n));
			builder.append("s");
			break;
		case NODE_TIME:
			for (Node e : c.getChildren()) {
				builder.append(visit_string(e));
				builder.append(".end");
			}
			builder.append("+");
			builder.append(visit_constante(n));
			builder.append("s");
			break;
		}
	}

	public void visit_expression_time_minus(Node n) {
		Node c = n.getChildren().get(0);
		switch (c.getType()) {
		case NODE_TIME_CLICK:
			builder.append(visit_string(c));
			builder.append("-");
			builder.append(visit_constante(n));
			builder.append("s");
			break;
		case NODE_TIME_VALUE:
			builder.append(visit_constante(c) - visit_constante(n));
			builder.append("s");
			break;
		case NODE_TIME:
			for (Node e : c.getChildren()) {
				builder.append(visit_string(e));
				builder.append(".end");
			}
			builder.append("-");
			builder.append(visit_constante(n));
			builder.append("s");
			break;
		}
	}

	public void visit_new_constant(Node n) {
		this.vista.addLocal(visit_string(n.getChildren().get(0)),
				new VariableSymbol(visit_string(n.getChildren().get(0)), visit_expression(n.getChildren().get(1))));
		// System.out.println(" k =
		// "+this.vista.topTable().get(visit_string(n.getChildren().get(0))).getValue());
	}

	public void visit_new_circle(Node n) {
		this.vista.addLocal(visit_string(n.getChildren().get(0)),
				new FunctionSymbol(visit_string(n.getChildren().get(0)), n.getChildren().get(1)));
		// System.out.println(this.vista.topTable().get(visit_string(n.getChildren().get(0))).getValue());
	}

	public void visit_new_rectangle(Node n) {
		this.vista.addLocal(visit_string(n.getChildren().get(0)),
				new FunctionSymbol(visit_string(n.getChildren().get(0)), n.getChildren().get(1)));
		// System.out.println(this.vista.topTable().get(visit_string(n.getChildren().get(0))).getName());
	}

	public void visit_loop(Node n) {
		for (int i = 0; i < visit_expression(n.getChildren().get(0)); i++) {
			for (int j = 1; j < n.getChildren().size(); j++) {
				visit(n.getChildren().get(j));
			}
		}
	}

	public void visit_color_rgb(Node n) {
		builder.append("rgb(");
		double r = visit_expression(n.getChildren().get(0));
		if (r > 255)
			r = 255;
		if (r < 0)
			r = 0;
		builder.append(r);
		builder.append(", ");
		double g = visit_expression(n.getChildren().get(1));
		if (g > 255)
			g = 255;
		if (g < 0)
			g = 0;
		builder.append(g);
		builder.append(", ");
		double b = visit_expression(n.getChildren().get(2));
		if (b > 255)
			b = 255;
		if (b < 0)
			b = 0;
		builder.append(b);
		builder.append(")");
	}

	public void visit_change_color(Node n) {
		int i = 0;
		builder.append("attributeName=\"fill\" ");
		for (Node e : n.getChildren()) {
			switch (i) {
			case 0:
				builder.append("from=\"");
				visit(e);
				builder.append("\" ");
				break;
			case 1:
				builder.append("to=\"");
				visit(e);
				builder.append("\" ");
				break;
			case 2:
				builder.append("dur=\"");
				builder.append(visit_expression(e));
				builder.append("s\"");
				break;
			default:
				builder.append(" begin=\"");
				visit(e);
				builder.append("\"");
				break;
			}
			i++;
		}
		builder.append(" fill=\"freeze\"/>\n");
	}

	public void visit_animation_color(Node n) {
		int i = 0;
		String id = "";
		for (Node e : n.getChildren()) {
			switch (i) {
			case 0:
				id = "xlink:href=\"#";
				id += visit_string(e);
				currentID = visit_string(e);
				id += "\" ";
				break;
			default:
				builder.append("<animate ");
				if (visit_string(n) != null) {
					builder.append("id=\"");
					builder.append(visit_string(n));
					builder.append("\" ");
				}
				builder.append(id);
				visit(e);
				break;
			}
			i++;
		}
	}
}
