package compiler;
import ast.svgFactory;
import ast.Node;
import Symbol.Symbol;
import java.util.ArrayList;


parser code {:
    public void report_error(String message, Object info) {
        StringBuffer m = new StringBuffer("Error");
		m.append (info.toString());
        m.append(" : "+message);
        System.err.println(m.toString());
    }
   
    public void report_fatal_error(String message, Object info) {
        report_error(message, info);
        System.exit(1);
    }
    
	private Context context;
    public void setContext (Context context) {
    	this.context = context;
    }

    public Context getContext () {
		return context;
    }

	public svgFactory getFactory () {
		return getContext().getFactory();
	}

:}

terminal 					NEW, PANEL;
terminal String				NAME;
terminal					EQUAL, TERM;
terminal					FILLED, WITH, STROKED;
terminal 					CIRCLE, RECTANGLE;
terminal					DRAW, ANIMATE;
terminal 					AT, AS;
terminal 					OPEN_BRACKETS, CLOSE_BRACKETS, OPEN_PARENTHESIS, CLOSE_PARENTHESIS;
terminal 					PLUS, MINUS, MULT, DIV;
terminal 					MOVE, FROM, TO, DURING, AFTER;
terminal String 			CLICK, HORIZONTAL, VERTICAL;
terminal double 			INT;
terminal String				TEXT;
terminal					REPEAT, TIMES;
terminal					RGB, COMA, COLOR, CHANGE;

nonterminal					Instructions, Init;
nonterminal	Node			Instruction;
nonterminal	Node			Initialisation, Creation, Drawing, Animation, Repeat;
nonterminal Node			NewShape, NewCircle, NewRectangle, NewConstant, Id;
nonterminal Node			Shape, Circle, Rectangle;
nonterminal Node 			Expression,ExpressionTime;
nonterminal Node			Action, Coordinates, Movement;
nonterminal	Node			Time, Event;
nonterminal String			As;
nonterminal Node			Fill, Stroke;
nonterminal Node 			Color, ChangeColor;
nonterminal ArrayList<Node>	Movements, InstructionLoop;

Init ::= Initialisation:i Instructions											{: getContext().addInitInstruction(i); :}
	 |	 Initialisation:i														{: getContext().addInitInstruction(i); :}
	 ;

Instructions ::= 																{: :}
			 |	Instructions Instruction:i										{: getContext().addGlobalInstruction(i); :}
			 ;	

Instruction ::= Initialisation:i												{: RESULT = i; :}
			|   Creation:c														{: RESULT = c; :}
			|   Drawing:d														{: RESULT = d; :}
			|   Animation:a														{: RESULT = a; :}
			|	Repeat:r														{: RESULT = r; :}
			;

Initialisation ::= NEW PANEL Expression:x Expression:y TERM						{: RESULT = getFactory().createNodeInitialisation(x,y); :}
			   ;

Creation ::= NewCircle:c														{: RESULT = c; :}
         |   NewRectangle:r														{: RESULT = r; :}
         |   NewConstant:c														{: RESULT = c; :}
         ;
 
Shape ::= Rectangle:r															{: RESULT = r; :}
      |   Circle:c																{: RESULT = c; :}
      |	  Text:t																{: RESULT = t :}
      |   Id:i																	{: RESULT = i; :}
      ;

Text ::= TEXT:t SIZE INT:i IN NAME:n											{: RESULT = getFactory().createNodeText(t, i, n); :}
     |	 TEXT:t SIZE INT:i														{: RESULT = getFactory().createNodeText(t, i); :}
     |   TEXT:t IN NAME:n														{: RESULT = getFactory().createNodeText(t); :}
     |	 TEXT:t																	{: RESULT = getFactory().createNodeText(t); :}
     ;

Id ::= NAME:n																	{: RESULT = getFactory().createNodeString(n); :}
   ;

NewShape ::= NewRectangle:r														{: RESULT = r; :}
         |   NewCircle:c														{: RESULT = c; :}
         ;

NewCircle ::= Id:i EQUAL Circle:c TERM											{: RESULT = getFactory().createNodeNewCircle(i, c); :}
          ;	

Circle ::= CIRCLE Expression:e Fill:f Stroke:s									{: RESULT = getFactory().createNodeCircle(e, f, s); :}
       ;

Fill ::=																		{: RESULT = null; :}
     |  FILLED WITH Color:c														{: RESULT = getFactory().createNewNodeFill(c); :}
     ;

Stroke ::=																		{: RESULT = null; :}
  	   |  STROKED WITH Color:c													{: RESULT = getFactory().createNewNodeStroke(c); :}
       ;

NewRectangle ::= Id:i EQUAL Rectangle:r TERM									{: RESULT = getFactory().createNodeNewRectangle(i, r); :}
             ;

Rectangle ::= RECTANGLE Expression:x Expression:y Fill:f Stroke:s				{: RESULT = getFactory().createNodeRectangle(x, y, f, s); :}
          ;

NewConstant ::= Id:i EQUAL Expression:e TERM									{: RESULT = getFactory().createNodeNewConstant(i, e); :}
            ;

Drawing ::= DRAW Shape:s AT Expression:x Expression:y As:i TERM					{: RESULT = getFactory().createNodeDrawing(s, x, y, i); :}
        ;

As ::= 																			{: RESULT = null; :}
   |  AS NAME:id																{: RESULT = id; :}
   ;

Expression ::= Expression:x PLUS Expression:y									{: RESULT = getFactory().createNodePlus(x, y); :}
           |   Expression:x MINUS Expression:y									{: RESULT = getFactory().createNodeMinus(x, y); :}
           |   Expression:x MULT Expression:y									{: RESULT = getFactory().createNodeMult(x, y); :}
           |   Expression:x DIV Expression:y									{: RESULT = getFactory().createNodeDiv(x, y); :}
           |   INT:x															{: RESULT = getFactory().createNodeConstante(x); :}
           |   Id:i																{: RESULT = i; :}
           ;

Animation ::= ANIMATE Id:i As:ia OPEN_BRACKETS Movements:m CLOSE_BRACKETS				{: RESULT = getFactory().createNodeAnimation(i, m, ia); :}
		  |   ANIMATE Id:i As:ia OPEN_BRACKETS ChangeColor:c CLOSE_BRACKETS			{: RESULT = getFactory().createNodeAnimationColor(i, c, ia); :}
          ;

ChangeColor ::= CHANGE COLOR FROM Color:c1 TO Color:c2 DURING Expression:i AFTER Event:e TERM	{: RESULT = getFactory().createNodeChangeColor(c1, c2, i, e); :}
            ;

Movements ::=   																{: RESULT = new ArrayList<Node>(); :}
          | Movements:list Movement:m											{: list.add(m); RESULT = list; :}
		  ;

Movement ::= Action:a DURING Expression:i AFTER Event:e TERM					{: RESULT = getFactory().createNodeMovement(a, i, e); :}
         ;

Action ::= MOVE Coordinates:c FROM Expression:e1 TO Expression:e2				{: RESULT = getFactory().createNodeAction(c, e1, e2); :}
	   |   MOVE Coordinates:c TO Expression:e									{: RESULT = getFactory().createNodeAction(c, e); :}
       ;

Coordinates ::= HORIZONTAL:s													{: RESULT = getFactory().createNodeCoordinates(s); :}
            |   VERTICAL:s														{: RESULT = getFactory().createNodeCoordinates(s); :}
            ;

Time ::= CLICK:s																{: RESULT = getFactory().createNodeTime(s); :}
     |	 Id:id																	{: RESULT = getFactory().createNodeTime(id); :}
     | 	 INT:i																	{: RESULT = getFactory().createNodeTime(i); :}
     ;

Event ::= Time:t																{: RESULT = getFactory().createNodeTime(t); :}
	  |   Time:t PLUS INT:i														{: RESULT = getFactory().createNodeExpressionTimePlus(t,i); :}
      |   Time:t MINUS INT:i													{: RESULT = getFactory().createNodeExpressionTimeMinus(t,i); :}
               ;

Repeat ::= REPEAT Expression:e TIMES OPEN_BRACKETS InstructionLoop:l CLOSE_BRACKETS						{: RESULT = getFactory().createLoop(e, l); :}
	   ;

InstructionLoop ::= InstructionLoop:list Instruction:i													{: list.add(i); RESULT = list; :}
				|																						{: RESULT = new ArrayList<Node>(); :}
				;

Color ::= RGB OPEN_PARENTHESIS Expression:e1 COMA Expression:e2 COMA Expression:e3 CLOSE_PARENTHESIS	{: RESULT = getFactory().createNodeColorRGB(e1, e2, e3); :}
      |   NAME:n																						{: RESULT = getFactory().createNodeColorName(n); :}
      ;